Une brève introduction à git
===
</BR>

# Configuration dépôt local
```
$ git init
Dépôt Git vide initialisé dans /home/sam/DATA/git/tutogit/.git/
```

```
$ ls -la 
drwxr-xr-x  3 sam sam 4096 mai   24 09:28 .
drwxr-xr-x 29 sam sam 4096 mai   24 09:24 ..
drwxr-xr-x  7 sam sam 4096 mai   24 09:28 .git
-rw-r--r--  1 sam sam   71 mai   24 09:27 README.md

```

```
 $ git add README.md    
 $ git status
Sur la branche master

Aucun commit

Modifications qui seront validées :
  (utilisez "git rm --cached <fichier>..." pour désindexer)

	nouveau fichier : README.md

```

```
$ git commit -m "Initial Commit: Ajout du Fichier Readme.md"
[master (commit racine) a29e44c] Initial Commit: Ajout du Fichier Readme.md
 1 file changed, 5 insertions(+)
 create mode 100644 README.md
```

```
$ git log
commit a29e44c37a004887a944908e52cbf4f34eea48af (HEAD -> master)
Author: sam <srisbourg@free.fr>
Date:   Wed May 24 09:35:40 2023 +0200

    Initial Commit: Ajout du Fichier Readme.md

```

```
$ git remote add origin git@gitlab.com:cnam_paysdelaloire2023/tutogit.git
```

```
$ git clone https://gitlab.com/cnam_paysdelaloire2023/tutogit2.git
Clonage dans 'tutogit2'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
Dépaquetage des objets: 100% (3/3), fait.
```

```
git status                                 130 ↵ ──(mer.,mai24)─┘
Sur la branche main
Votre branche est à jour avec 'origin/main'.

Modifications qui ne seront pas validées :
  (utilisez "git add <fichier>..." pour mettre à jour ce qui sera validé)
  (utilisez "git checkout -- <fichier>..." pour annuler les modifications dans la copie de travail)

	modifié :         README.md

aucune modification n'a été ajoutée à la validation (utilisez "git add" ou "git commit -a")

```

```
$ git diff README.md 
diff --git a/README.md b/README.md
index 64a05be..74c4880 100644
--- a/README.md
+++ b/README.md
@@ -1,92 +1,63 @@
-# tutogit2
-
-
-
-## Getting started
+Une brève introduction à git
+===
+</BR>
 
+# Configuration dépôt local

```

```
git add README.md                              ──(mer.,mai24)─┘
┌─(~/DATA/git/tutogit2)──────────────────────────────────────────(sam@hephaistos:pts/0)─┐
└─(10:04:07 on main ✚)──> git commit -m "Nouvelle version du README.md"  ──(mer.,mai24)─┘
[main af74f67] Nouvelle version du README.md
 1 file changed, 63 insertions(+), 92 deletions(-)
 rewrite README.md (99%)
┌─(~/DATA/git/tutogit2)──────────────────────────────────────────(sam@hephaistos:pts/0)─┐
└─(10:04:22 on main)──> git push                                         ──(mer.,mai24)─┘
Username for 'https://gitlab.com': srisbourg@free.fr
Password for 'https://srisbourg@free.fr@gitlab.com': 
Énumération des objets: 5, fait.
Décompte des objets: 100% (5/5), fait.
Compression par delta en utilisant jusqu'à 8 fils d'exécution
Compression des objets: 100% (2/2), fait.
Écriture des objets: 100% (3/3), 965 bytes | 965.00 KiB/s, fait.
Total 3 (delta 0), réutilisés 0 (delta 0)
To https://gitlab.com/cnam_paysdelaloire2023/tutogit2.git
   ab6e4aa..af74f67  main -> main

```

```
git status                                 130 ↵ ──(mer.,mai24)─┘
Sur la branche main
Votre branche est à jour avec 'origin/main'.

Modifications qui ne seront pas validées :
  (utilisez "git add <fichier>..." pour mettre à jour ce qui sera validé)
  (utilisez "git checkout -- <fichier>..." pour annuler les modifications dans la copie de travail)

	modifié :         README.md

aucune modification n'a été ajoutée à la validation (utilisez "git add" ou "git commit -a")

```

```
$ git diff README.md 
diff --git a/README.md b/README.md
index 64a05be..74c4880 100644
--- a/README.md
+++ b/README.md
@@ -1,92 +1,63 @@
-# tutogit2
-
-
-
-## Getting started
+Une brève introduction à git
+===
+</BR>
 
+# Configuration dépôt local

```

```
git add README.md                              ──(mer.,mai24)─┘
┌─(~/DATA/git/tutogit2)──────────────────────────────────────────(sam@hephaistos:pts/0)─┐
└─(10:04:07 on main ✚)──> git commit -m "Nouvelle version du README.md"  ──(mer.,mai24)─┘
[main af74f67] Nouvelle version du README.md
 1 file changed, 63 insertions(+), 92 deletions(-)
 rewrite README.md (99%)
┌─(~/DATA/git/tutogit2)──────────────────────────────────────────(sam@hephaistos:pts/0)─┐
└─(10:04:22 on main)──> git push                                         ──(mer.,mai24)─┘
Username for 'https://gitlab.com': srisbourg@free.fr
Password for 'https://srisbourg@free.fr@gitlab.com': 
Énumération des objets: 5, fait.
Décompte des objets: 100% (5/5), fait.
Compression par delta en utilisant jusqu'à 8 fils d'exécution
Compression des objets: 100% (2/2), fait.
Écriture des objets: 100% (3/3), 965 bytes | 965.00 KiB/s, fait.
Total 3 (delta 0), réutilisés 0 (delta 0)
To https://gitlab.com/cnam_paysdelaloire2023/tutogit2.git
   ab6e4aa..af74f67  main -> main

```
# Exemple de modification

# Fonctionnalités Loto

1. Brancher une BDD
   -Analyse minimale sur le MCD (schéma de la base)
     - On souhiate pouvoir créer un nouveau jeu
        - Générer les cartons
        - Conserver historique du tirage
        - Enregistrer le/les joueurs
        - Historique gagnant

2. Serveur multijoueurs
  - Symfony -> Chercher comment faire 
  - Projet HTML côté client -> évoluer vers une architecture avec un serveur
    - nodejs -> Chercher comment faire 
    - django -> Chercher comment faire 

3. Service Web/ API

4. Amélioration côté client / Différentes version
  - Remplissage automatique du carton client selon le tirage du serveur.
  - Le joueur coche lui même sur ses cartons le chiffre tiré si il y a lieu.
    En cas d'oubli, on peut le signaler au joueur mais interdire de cocher la case
    du carton rétroactivement.

# URL d'hébergement du site sur les pages gitlab
- https://cnam_paysdelaloire2023.gitlab.io/tutogit2/
- https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html
- ? hébergement backend sur les pages ?
